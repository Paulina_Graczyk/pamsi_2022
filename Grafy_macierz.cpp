﻿#include <iostream>

//////////////////////////////////////////GRAFY NA PODSTAWIE MACIERZY SASIEDZTWA////////////////////////////////////
using namespace std;

const int SIZE_WIERZ = 5; //ilosc wierzchołków
const double GESTOSC = 1.0; //gestosc grafu



struct lista_wierz;
struct lista_kraw;
struct wierzcholek;
struct krawedz;

struct lista_wierz
{
    wierzcholek* wsk_wierz;
    lista_wierz* nastepny;
    lista_wierz(wierzcholek* nowy_wierz, lista_wierz* head) { //konstruktor
        this->wsk_wierz = nowy_wierz;
        this->nastepny = head;
    }

    
};

int ile_wierzch(lista_wierz* head) {
    int i = 1;
    lista_wierz* temp = head;
    while (temp->nastepny != NULL) {
        i++;
        temp = temp->nastepny;
    }
    return i;
}

struct lista_kraw
{
    krawedz* wsk_kraw;
    lista_kraw* nastepny;

    lista_kraw(krawedz* nowa_kraw, lista_kraw* head) { //konstruktor
        this->wsk_kraw = nowa_kraw;
        this->nastepny = head;
    }



};

int ile_kraw(lista_kraw* head) {
    int i = 1;
    lista_kraw* temp = head;
    while (temp->nastepny != NULL) {
        i++;
        temp = temp->nastepny;
    }
    return i;
}

struct wierzcholek
{
    int elem;
    int key;
    lista_wierz* pozycja_l_wierz;

    wierzcholek(int wart, int nowy_key) { //konstruktor
        this->elem = wart;
        this->key = nowy_key;
        this->pozycja_l_wierz = NULL;
        
    }


    void replace(int new_elem) { //zamienia element w wierzchołku na nowy
        this->elem = new_elem;
    }

};

struct krawedz
{
    int waga;
    int w1, w2;
    lista_kraw* pozycja_l_kraw;
    wierzcholek* wierz_pocz;
    wierzcholek* wierz_konc;

    krawedz(int new_waga, wierzcholek* pocz, wierzcholek* koniec) { //konstruktor konstruktor
        this->waga = new_waga;
        this->pozycja_l_kraw = NULL;
        this->wierz_pocz = pocz;
        this->wierz_konc = koniec;

    }
    krawedz() {
        this->waga = 0;
        this->pozycja_l_kraw = NULL;
        this->wierz_pocz = NULL;
        this->wierz_konc = NULL;
    }

    wierzcholek** endVertices() { //zwraca tablice dwóch wskażników końcowych wierzchołków krawedzi
        wierzcholek* tab[2];
        tab[0] = wierz_pocz;
        tab[1] = wierz_konc;
        return tab;
    }
};

krawedz* macierz_s[SIZE_WIERZ][SIZE_WIERZ];  //tworzenie macierzy sasiedztwa

//////////////////////////////////////////////////////////////////////////////////////////
//Kolejka priorytetowa
class Kolejka
{
private:
    krawedz* Heap;
    int hpos;
public:
    Kolejka(int n);
    ~Kolejka();
    krawedz front();
    void push(krawedz e);
    void pop();
};

// Obiekt struktury klastry
struct ob_klastry
{
    int up, rank;
};

class Klastry
{
private:
    ob_klastry* Z;
public:
    Klastry(int n);
    ~Klastry();
    void Stworz_kl(int v);
    int Znajdz_kl(int v);
    void Polacz_kl(krawedz e);
};

// Obiektu minimalnego drzewa rozpinającego
struct ob_drzewo
{
    ob_drzewo* next;
    int v, waga;
};

class Drzewo
{
private:
    ob_drzewo** A;            // Tablica list sąsiedztwa
    int len_list_sas;              // Liczba komórek w tablicy
    int waga;            // Waga całego drzewa
public:
    Drzewo(int n);
    ~Drzewo();
    void addkrawedz(krawedz e);
    ob_drzewo* get_list_sas(int n);
};

// Definicje metod obiektu Kolejka
// Konstruktor - tworzy n elementową tablicę heap na kopiec
Kolejka::Kolejka(int n)
{
    Heap = new krawedz[n];   // Tworzymy tablicę
    hpos = 0;                // Pozycja w kopcu
}

// Destruktor - usuwa kopiec z pamięci
Kolejka::~Kolejka()
{
    delete[] Heap;
}

// Zwraca krawędź z początku kopca
krawedz Kolejka::front()
{
    return Heap[0];
}

// Umieszcza w kopcu nową krawędź i odtwarza strukturę kopca
void Kolejka::push(krawedz e)
{
    int i, j;

    i = hpos++;              // i ustawiamy na koniec kopca
    j = (i - 1) >> 1;      

    // Szukamy miejsca w kopcu dla e

    while (i && (Heap[j].waga > e.waga))
    {
        Heap[i] = Heap[j];
        i = j;
        j = (i - 1) >> 1;
    }

    Heap[i] = e;          // Krawędź e wstawiamy do kopca
}

// Usuwa korzeń z kopca i odtwarza jego strukturę
void Kolejka::pop()
{
    int i, j;
    krawedz e;

    if (hpos)
    {
        e = Heap[--hpos];

        i = 0;
        j = 1;

        while (j < hpos)
        {
            if ((j + 1 < hpos) && (Heap[j + 1].waga < Heap[j].waga)) j++;
            if (e.waga <= Heap[j].waga) break;
            Heap[i] = Heap[j];
            i = j;
            j = (j << 1) + 1;
        }

        Heap[i] = e;
    }
}

// Definicje metod obiektu Klastry

// Konstruktor
Klastry::Klastry(int n)
{
    Z = new ob_klastry[n];    // Tworzymy tablicę dla elementów zbiorów
}

// Destruktor
Klastry::~Klastry()
{
    delete[] Z;            // Usuwamy tablicę ze zbiorami
}

// Tworzy wpis w tablicy Z
void Klastry::Stworz_kl(int v)
{
    Z[v].up = v;
    Z[v].rank = 0;
}

// Zwraca indeks reprezentanta zbioru, w którym jest wierzchołek v
int Klastry::Znajdz_kl(int v)
{
    if (Z[v].up != v) Z[v].up = Znajdz_kl(Z[v].up);
    return Z[v].up;
}

// Łączy ze sobą klastry
void Klastry::Polacz_kl(krawedz e)
{
    int ru, rv;

    ru = Znajdz_kl(e.wierz_pocz->elem);   
    rv = Znajdz_kl(e.wierz_konc->elem);   
    if (ru != rv)           // Korzenie muszą być różne
    {
        if (Z[ru].rank > Z[rv].rank) // Porównujemy rangi drzew
            Z[rv].up = ru;    // ru większe, dołączamy rv
        else
        {
            Z[ru].up = rv;    // równe lub rv większe, dołączamy ru
            if (Z[ru].rank == Z[rv].rank) Z[rv].rank++;
        }
    }
}

// Definicje metod obiektu Drzewo

// Konstruktor - tworzy tablicę pustych list sąsiedztwa
Drzewo::Drzewo(int n)
{
    int i;

    A = new ob_drzewo * [n];   // Tworzymy tablicę dynamiczną
    for (i = 0; i < n; i++) A[i] = NULL; // i wypełniamy ją pustymi listami
    len_list_sas = n - 1;            // Zapamiętujemy długość tablicy
    waga = 0;              // Zerujemy wagę drzewa
}

// Destruktor - usuwa listy oraz tablicę sąsiedztwa
Drzewo::~Drzewo()
{
    int i;
    ob_drzewo* p, * r;

    for (i = 0; i <= len_list_sas; i++)
    {
        p = A[i];
        while (p)
        {
            r = p;               // Zapamiętujemy wskazanie
            p = p->next;         // Przesuwamy się do następnego elementu listy
            delete r;            // Usuwamy element
        }
    }

    delete[] A;            // Usuwamy tablicę list sąsiedztwa
}

// Dodaje krawędź do drzewa
void Drzewo::addkrawedz(krawedz e)
{
    ob_drzewo* p;

    waga += e.waga;      // Do wagi drzewa dodajemy wagę krawędzi
    p = new ob_drzewo;           // Tworzymy nowy węzeł
    p->v = e.wierz_konc->elem;             // Wierzchołek końcowy
    p->waga = e.waga;    // Waga krawędzi
    p->next = A[e.wierz_pocz->elem];    // Dodajemy p do listy wierzchołka wierz_pocz->elem
    A[e.wierz_pocz->elem] = p;

    p = new ob_drzewo;           // To samo dla krawędzi odwrotnej
    p->v = e.wierz_pocz->elem;             // Wierzchołek końcowy
    p->waga = e.waga;    // Waga krawędzi
    p->next = A[e.wierz_konc->elem];    // Dodajemy p do listy wierzchołka wierz_konc->elem
    A[e.wierz_konc->elem] = p;
}

// Zwraca wskaźnik początku listy sąsiadów wierzchołka
ob_drzewo* Drzewo::get_list_sas(int n)
{
    return A[n];
}









//////////////////////////////////////////////////////////////////////////////////////////
//Algorytm Kruskala
void Kruskala(lista_kraw* lista_krawedzi, lista_wierz* lista_wierzcholkow) {
    

    lista_kraw* temp = lista_krawedzi;
    int n, m;                // Liczba wierzchołków i krawędzi
    krawedz e;
    int i;

    n = ile_wierzch(lista_wierzcholkow);
    m = ile_kraw(lista_krawedzi);

    Klastry Z(n);        // Klastry
    Kolejka Q(m);           // Kolejka priorytetowa 
    Drzewo T(n);          // Minimalne drzewo rozpinające

    for (i = 0; i < n; i++)
        Z.Stworz_kl(i);       // Dla każdego wierzchołka tworzymy osobny klaster

    for (i = 0; i < m; i++)
    {
        e = *(temp->wsk_kraw);
        temp = temp->nastepny;
        Q.push(e);          // krawedzie umieszczamy je w kolejce priorytetowej
    }

    for (i = 1; i < n; i++) 
    {
        do
        {
            e = Q.front();      // Pobieramy z kolejki krawędź
            Q.pop();            // Krawędź usuwamy z kolejki
        } while (Z.Znajdz_kl(e.wierz_pocz->elem) == Z.Znajdz_kl(e.wierz_konc->elem)); //dopuki nie są w różnych zbiorach
        T.addkrawedz(e);       // Dodajemy krawędź do drzewa
        cout << "Krawedz " << e.wierz_pocz->elem << " - " << e.wierz_konc->elem << " waga:" << e.waga << endl;
        Z.Polacz_kl(e);     //  łączymy ze sobą klastry
    }

}
//////////////////////////////////////////////////////////////////////////////////////////
//Agorytm Prima
void Prima(lista_kraw* lista_krawedzi, lista_wierz* lista_wierzcholkow) {

    lista_kraw* temp = lista_krawedzi;
    int n, m;                      // Liczba wierzchołków i krawędzi
    krawedz e;
    ob_drzewo* p;
    int i, v;
    int wierzcholki=1;
    int wiersz = 0;

    n = ile_wierzch(lista_wierzcholkow);
    m = ile_kraw(lista_krawedzi);

    Kolejka Q(m);                 // Kolejka priorytetowa 
    Drzewo T(n);                // Minimalne drzewo rozpinające
    bool* tab_odwiedzin = new bool[n];

    for (i = 0; i < n; i++)
        tab_odwiedzin[i] = false;       //  tablica odwiedzin


    // Tworzymy minimalne drzewo rozpinające

    tab_odwiedzin[0] = true; //wierzchołek startowy odwiedzony
    while (wierzcholki != n)  // n liczba wierzcholkow
    {
        for (i = 0; i < n; i++)//przeszukanie pierwszego wiersza
        {
            if (macierz_s[wiersz][i] != 0)//jezeli nie ma polaczenia z wierzcholkiem to nie wchodz
            {
                if (tab_odwiedzin[i] == false)// jezeli byl odwiedzony to nie wchodz
                {
                    
                    
                    e = *macierz_s[wiersz][i];
                    e.w1 = wiersz;                // to tworzymy krawędź
                    e.w2 = i;
                    e.wierz_pocz->elem = wiersz;
                    e.wierz_konc->elem = i;
                    Q.push(e);
                }
            }
        }

        do
        {
            e = Q.front();            // Pobieramy krawędź z kolejki
            Q.pop();
        } while (tab_odwiedzin[e.w2]); 

        T.addkrawedz(e);             // Dodajemy krawędź do drzewa rozpinającego
       
        cout << "Krawedz " << e.w1 << " - " << e.w2 << " waga:" << e.waga << endl;
        
        tab_odwiedzin[e.w2] = true;     // Oznaczamy drugi wierzchołek jako odwiedzony
        wierzcholki++;
        wiersz = e.w2;
        
    }

    delete[] tab_odwiedzin;

}


//////////////////////////////////////////////////////////////////////////////////////////

void stworz_graf(lista_wierz & head_wierz, lista_kraw & head_kraw) {

    for (int i = 0; i < (SIZE_WIERZ); i++) {
        for (int j = 0; j < SIZE_WIERZ; j++) {
            macierz_s[i][j] = NULL; //wypelnij NULLami macierz sasiedztwa
        }
    }

    srand((int)time(0));
    int n = SIZE_WIERZ;
    int m_max = (n * (n - 1)) / 2;
    int m = GESTOSC * m_max;
    int pom_wag;

    lista_wierz* wsk_wierz_2 = new lista_wierz(NULL, NULL);
    lista_kraw* wsk_kraw_2 = new lista_kraw(NULL, NULL);

    wierzcholek* nowy_wierz = new wierzcholek(0, 0);   //tworzenie pierwszego wierzchołka
    lista_wierz* wsk_wierz = new lista_wierz(nowy_wierz, NULL);
    nowy_wierz->pozycja_l_wierz = wsk_wierz;

    for (int i = 1; i < n; i++) {   //tworzenie reszty wierzcholkow

        nowy_wierz = new wierzcholek(i, i);
        wsk_wierz_2 = new lista_wierz(nowy_wierz, wsk_wierz);
        nowy_wierz->pozycja_l_wierz = wsk_wierz_2;
        wsk_wierz = wsk_wierz_2;
    }

    head_wierz.wsk_wierz = wsk_wierz_2->wsk_wierz;
    head_wierz.nastepny = wsk_wierz_2->nastepny;
    lista_wierz* pom = wsk_wierz_2;

   cout << "Ilosc wierzcholkow w grafie:" << ile_wierzch(wsk_wierz_2) << endl;


    pom_wag = rand() % 100;
    krawedz* nowa_kraw = new krawedz(pom_wag, wsk_wierz_2->nastepny->wsk_wierz, wsk_wierz_2->wsk_wierz);  //tworzenie pierwszej krawedzi
    macierz_s[wsk_wierz_2->nastepny->wsk_wierz->key][wsk_wierz->wsk_wierz->key] = nowa_kraw;    // przypisanie do macierzy sasiedztwa
    macierz_s[wsk_wierz->wsk_wierz->key][wsk_wierz_2->nastepny->wsk_wierz->key] = nowa_kraw;
    wsk_wierz_2 = wsk_wierz_2->nastepny;
    wsk_wierz = wsk_wierz->nastepny;
    lista_kraw* wsk_kraw = new lista_kraw(nowa_kraw, NULL);
    nowa_kraw->pozycja_l_kraw = wsk_kraw;
    

    while(wsk_wierz_2->nastepny!=NULL) {   //tworzenie reszty krawedzi zeby graf byl spojny
        pom_wag = rand() % 100;
        nowa_kraw = new krawedz(pom_wag, wsk_wierz_2->nastepny->wsk_wierz, wsk_wierz_2->wsk_wierz);
        macierz_s[wsk_wierz_2->nastepny->wsk_wierz->key][wsk_wierz->wsk_wierz->key] = nowa_kraw;    // przypisanie do macierzy sasiedztwa
        macierz_s[wsk_wierz->wsk_wierz->key][wsk_wierz_2->nastepny->wsk_wierz->key] = nowa_kraw;
        wsk_wierz_2 = wsk_wierz_2->nastepny;
        wsk_wierz = wsk_wierz->nastepny;
        wsk_kraw_2 = new lista_kraw(nowa_kraw, wsk_kraw);
        nowa_kraw->pozycja_l_kraw = wsk_kraw_2;
        wsk_kraw = wsk_kraw_2;
    }

    /*Tworzenie reszty krawedzi*/
    lista_wierz* wsk_wie_1 = new lista_wierz(NULL, NULL);
    lista_wierz* wsk_wie_2 = new lista_wierz(NULL, NULL);
    lista_kraw* wsk_kraw_3 = new lista_kraw(NULL, NULL);
    lista_wierz* temp = new lista_wierz(NULL, NULL);
    lista_wierz* temp2 = new lista_wierz(NULL, NULL);
    int wierz_1;
    int wierz_2;
    int l_kraw=n-1; //liczba dotychczas utworzonych krawedzi czyli o jedna mniej niz licz. wierzcholkow

    while (l_kraw<m) {
        
         wierz_1 = rand() % SIZE_WIERZ;  // wylosuj 2 numery wierzcholków (klucze)
         wierz_2 = rand() % SIZE_WIERZ;

        while (wierz_2 == wierz_1) {
            wierz_2 = rand() % SIZE_WIERZ;
        }


        if (macierz_s[wierz_1][wierz_2] == NULL && macierz_s[wierz_2][wierz_1]==NULL) {    //jesli nie sa juz polączone 
            temp = pom;
            temp2 = pom;
            while (temp->wsk_wierz->key != wierz_1) {   //wyszukaj wierzchołki o tym numerze
                temp = temp->nastepny;
            }
            wsk_wie_1 = temp;

            while (temp2->wsk_wierz->key != wierz_2) {
                temp2 = temp2->nastepny;
            }
            wsk_wie_2 = temp2;

            pom_wag = rand() % 100;
            nowa_kraw = new krawedz(pom_wag, wsk_wie_1->wsk_wierz, wsk_wie_2->wsk_wierz);   //stworz krawedz
            macierz_s[wierz_1][wierz_2] = nowa_kraw;    // przypisanie do macierzy sasiedztwa
            macierz_s[wierz_2][wierz_1] = nowa_kraw;
            wsk_kraw_3 = new lista_kraw(nowa_kraw, wsk_kraw_2);
            nowa_kraw->pozycja_l_kraw = wsk_kraw_3;
            wsk_kraw_2 = wsk_kraw_3;

            l_kraw++;
        }
    }

    head_kraw.wsk_kraw = wsk_kraw_2->wsk_kraw;
    head_kraw.nastepny = wsk_kraw_2->nastepny;

    cout << "Ilosc krawedzi w grafie: " << ile_kraw(wsk_kraw_2) << endl;
}

//////////////////////////////////////////////////////////////////////////////////////////

int main() {

    lista_wierz head_wierzcholki(NULL, NULL);
    lista_kraw head_krawedzie(NULL, NULL);


    //clock_t start = clock();
    //for (int i = 0; i < 100; i++) {
    //    head_wierzcholki.wsk_wierz = NULL;
    //    head_wierzcholki.nastepny = NULL;
    //    head_krawedzie.wsk_kraw = NULL;
    //    head_krawedzie.nastepny = NULL;
    //    stworz_graf(head_wierzcholki, head_krawedzie);
    //    Prima(&head_krawedzie, &head_wierzcholki);
    //}
    //clock_t stop = clock();
    //double time = double(stop - start) / CLOCKS_PER_SEC; 
    //cout << "czas wykonywania: " << time << endl;

 
    stworz_graf(head_wierzcholki, head_krawedzie);
    Kruskala(&head_krawedzie, &head_wierzcholki);
    



    return 0;
}