#include "AI.h"

//Umieszcza znak w zadanym miejscu na planszy
void c_put(board* p_board)
{
	turn c_move;
	c_move = best_move(p_board);
	p_board->b_pointer[c_move.verse][c_move.column] = enemy;
}

//Algorytm minimax przy zadanej glebokosci i uzywajac ciecia alfa-beta.
int minimax(board* p_b, int depth, bool is_maximizing, int alfa, int beta)
{
	int effect = p_b->check_who_wins();
	if (effect == 1){
		return effect;
	}

	if (effect == -1){
		return effect;
	}

	if (p_b->is_move_left() == false || depth == 0){
		return 0;
	}

	if (is_maximizing){
		int best = n_inf;
		for (int i = 0; i < p_b->b_size; i++) {
			for (int j = 0; j < p_b->b_size; j++) {
				if (p_b->b_pointer[i][j] == ' ') {
					p_b->b_pointer[i][j] = player;
					int score = minimax(p_b, depth - 1, !is_maximizing, alfa, beta);
					p_b->b_pointer[i][j] = ' ';
					best = std::max(score, best);
					alfa = std::max(alfa, score);
					if (beta <= alfa) break;
				}
			}
		}
		return best;
	}

	else{
		int best = inf;
		for (int i = 0; i < p_b->b_size; i++){
			for (int j = 0; j < p_b->b_size; j++){
				if (p_b->b_pointer[i][j] == ' '){
					p_b->b_pointer[i][j] = enemy;
					int score = minimax(p_b, depth - 1, !is_maximizing, alfa, beta);
					p_b->b_pointer[i][j] = ' ';
					best = std::min(score, best);
					 beta = std::min(beta, score);
					if (beta <= alfa) break;
				}
			}
		}
		return best;
	}
}


//Szuka najlepszego ruchu w danym momencie rozgrywki
turn best_move(board* p_b)
{
	int best_val = inf;
	turn best;
	best.verse = -1;
	best.column = -1;
	int depth = depth_2(p_b);

	for (int i = 0; i < p_b->b_size; i++){
		for (int j = 0; j < p_b->b_size; j++){
			if (p_b->b_pointer[i][j] == ' '){
				p_b->b_pointer[i][j] = enemy;
				int move = minimax(p_b, depth, true, n_inf, inf);
				p_b->b_pointer[i][j] = ' ';

				if (move < best_val){
					best_val = move;
					best.verse = i;
					best.column = j;
				}
			}
		}
	}
	return best;
}


//Wybiera glebokosc szukania dla danego rozmiaru planszy
int depth_2(board* p_brd) {
	int size = p_brd->b_size;
	if (size == 3) return 9;
	else if (size >= 4 && size < 7) return (9 - size + 1);
	else if (size >= 7 && size < 10) return 3;
	else return 2;
}