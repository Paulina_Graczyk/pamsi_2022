#include "Board.h"
using namespace std;

//Konstruktor 
board::board(int size, int scope) {
	this->c_scope = scope;
	this->b_size = size;
	this->b_pointer = new char* [this->b_size];

	for (int i = 0; i < this->b_size; i++) {
		this->b_pointer[i] = new char[this->b_size];
	}

	for (int i = 0; i < this->b_size; i++) {
		for (int j = 0; j < this->b_size; j++) {
			this->b_pointer[i][j] = ' ';
		}
	}
}

//Destruktor
board::~board() {
	for (int i = 0; i < this->b_size; i++) {
		delete[] this->b_pointer[i];
	}
}

//Rysuje plansze w terminalu
void board::show() {
	for (int i = 0; i < this->b_size; i++) {
		for (int j = 0; j < this->b_size; j++) {
			cout << ' ' << this->b_pointer[i][j] << ' ';
			if (j != this->b_size - 1) cout << "|";
		}

		if (i != this->b_size - 1) {
			cout << endl;
			for (int k = 0; k < this->b_size + this->b_size - 1; k++) {
				if (k % 2 == 1) cout << "+";
				else if (k % 2 == 0) cout << "---";
			}
			cout << endl;
		}
	}
	cout << endl;
}

//Sprawdza czy zosta� jakikolwiek ruch do wykonania
bool board::is_move_left() {
	int count = 0;
	for (int i = 0; i < this->b_size; i++) {
		for (int j = 0; j < this->b_size; j++) {
			if (this->b_pointer[i][j] == ' ') {
				count++;
			}
		}
	}
	if (count > 0) return true;
	else return false;
}


//Sprawdza czy ktorys z graczy umiescil wygrywajaca ilosc znakow w rz�dzie. 
//Zwraca 1 w przypadku zwyciestwa X
//Zwraca -1 w przypadku zwyciestwa O
//Zwraca 0 w kazdym innym przypadku
int board::check_who_wins()
{
	if (this->c_scope == this->b_size){		//jesli wygrywa tyle znak�w co rozmiar tablicy
		if (this->b_pointer[0][0] != ' '){
			for (int i = 1; i < this->b_size; i++){		//po skosie w prawo
				if (this->b_pointer[0][0] != this->b_pointer[i][i]){
					break;
				}
				else if (i == this->b_size - 1){
					if (this->b_pointer[0][0] == player) return 1;
					else if (this->b_pointer[0][0] == enemy) return -1;
				}
			}
		}

		for (int j = 0; j < this->b_size; j++){
			if (this->b_pointer[0][j] != ' '){
				for (int i = 0; i < this->b_size; i++) {		//kolumny
					if (this->b_pointer[0][j] != this->b_pointer[i][j]){
						break;
					}
					else if (i == this->b_size - 1){
						if (this->b_pointer[0][j] == player) return 1;
						else if (this->b_pointer[0][j] == enemy) return -1;
					}
				}
			}
		}

		for (int i = 0; i < this->b_size; i++){
			if (this->b_pointer[i][0] != ' '){
				for (int j = 0; j < this->b_size; j++) {	//wiersze
					if (this->b_pointer[i][0] != this->b_pointer[i][j]){
						break;
					}
					else if (j == this->b_size - 1){
						if (this->b_pointer[i][0] == player) return 1;
						else if (this->b_pointer[i][0] == enemy) return -1;
					}
				}
			}
		}

		if (this->b_pointer[0][this->b_size - 1] != ' '){	//po skosie w lewo{
			int j = this->b_size - 1;
			for (int i = 0; i < this->b_size; i++){
				if (this->b_pointer[0][this->b_size - 1] != this->b_pointer[i][j - i]) {
					break;
				}
				else if (i == this->b_size - 1){
					if (this->b_pointer[0][this->b_size - 1] == player) return 1;
					else if (this->b_pointer[0][this->b_size - 1] == enemy) return -1;
				}
			}
		}
	}

	else {		//gdy wygrywa inna ilosc niz rozmiar
		int count = this->b_size - this->c_scope;
		for (int i = 0; i <= count; i++){
			for (int j = 0; j <= count; j++){
				if (this->b_pointer[i][j] != ' '){
					for (int k = 0; k < this->c_scope; k++){
						if (this->b_pointer[i][j] != this->b_pointer[i + k][j + k]) break;

						else if (k == this->c_scope - 1){
							if (this->b_pointer[i][j] == player) return 1;
							else if (this->b_pointer[i][j] == enemy) return -1;
						}
					}
				}
			}
		}

		for (int i = 0; i <= count; i++){
			for (int j = 0; j < this->b_size; j++){
				if (this->b_pointer[i][j] != ' '){
					for (int k = 0; k < this->c_scope; k++){
						if (this->b_pointer[i][j] != this->b_pointer[i + k][j]) break;

						else if (k == this->c_scope - 1){
							if (this->b_pointer[i][j] == player) return 1;
							else if (this->b_pointer[i][j] == enemy) return -1;
						}
					}
				}
			}
		}

		for (int i = 0; i < this->b_size; i++){
			for (int j = 0; j <= count; j++){
				if (this->b_pointer[i][j] != ' '){
					for (int k = 0; k < this->c_scope; k++){
						if (this->b_pointer[i][j] != this->b_pointer[i][j + k]) break;

						else if (k == this->c_scope - 1){
							if (this->b_pointer[i][j] == player) return 1;
							else if (this->b_pointer[i][j] == enemy) return -1;
						}
					}
				}
			}
		}

		for (int i = 0; i <= count; i++){
			for (int j = this->b_size - 1; j >= count; j--){
				if (this->b_pointer[i][j] != ' '){
					for (int k = 0; k < this->c_scope; k++){
						if (this->b_pointer[i][j] != this->b_pointer[i + k][j - k]) break;

						else if (k == this->c_scope - 1){
							if (this->b_pointer[i][j] == player) return 1;
							else if (this->b_pointer[i][j] == enemy) return -1;
						}
					}
				}
			}
		}
	}

	return 0;
}