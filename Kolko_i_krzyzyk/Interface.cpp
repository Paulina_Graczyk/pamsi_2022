#include "Interface.h"
using namespace std;

int enter_size() {
	int size;
	cout << "Podaj rozmiar planszy: ";
	cin >> size;

	if (size < min_size_board) {
		cout << "Podano za małą liczbę! Minimalny rozmiar to 3 \n";
		size = enter_size();
	}
	return size;
}

void move_statement(int circular) {
	if (circular % 2 == 1) {
		cout << "---------------------------\n";
		cout << "       Ruch gracza X       \n";
		cout << "---------------------------\n";
	}
	else {
		cout << "--------------------------\n";
		cout << "       Ruch gracza O      \n";
		cout << "--------------------------\n";
	}
}

int enter_scope() {
	int scope;
	cout << "Podaj ilosc wygrywajacych znakow w rzedzie: ";
	cin >> scope;

	if (scope < min_scope) {
		cout << "Podano za małą liczbę! Minimalna ilosc znakow to 3 \n";
		scope = enter_scope();
	}
	return scope;
}

void statement_end(board* p_brd, int circular) {
	if (p_brd->check_who_wins() == 1 || p_brd->check_who_wins() == -1) {
		if (circular % 2 == 1) {
			cout << "---------------------------------------\n";
			cout << "             Gracz X wygrywa!          \n";
			cout << "---------------------------------------";
		}
		else {
			cout << "---------------------------------------\n";
			cout << "             Gracz O wygrywa!          \n";
			cout << "---------------------------------------";
		}
	}
	else {
		cout << "---------------------------------------\n";
		cout << "                 Remis!                \n";
		cout << "---------------------------------------";
	}
}

int menu() {
	int selection;
	cout << "--------------------------------------------------\n";
	cout << "                   Kolko i krzyzyk                \n";
	cout << "--------------------------------------------------\n";
	cout << "1. gracz vs gracz \n";
	cout << "2. gracz vs komputer \n";
	cout << "> ";
	cin >> selection;

	if (selection == 1 || selection == 2) return selection;
	else {
		cout << "Niepoprawna opcja! Sprobuj ponownie \n";
		selection = menu();
	}
}