#pragma once
#include "Board.h"

int enter_size();
int enter_scope();
void move_statement(int circular);
int menu();
void statement_end(board* brd, int circular);