#include "player.h"
using namespace std;

//Pobiera od gracza ruch.
//Wychwytuje wyjscie poza granice planszy oraz miejsce w ktorym stoi juz jakis inny znak.
turn enter_turn(board* brd) {
	turn p_turn;
	cin >> p_turn;

	if (p_turn.verse < brd->b_size && p_turn.column < brd->b_size) {
		if (brd->b_pointer[p_turn.verse][p_turn.column] != ' ') {
			cout << "Pole juz zajete, sprobuj ponownie! \n";
			p_turn = enter_turn(brd);
		}
	}
	else {
		cout << "Poza plansza, sprobuj ponownie! \n";
		p_turn = enter_turn(brd);
	}

	return p_turn;
}


//Umieszcza na planszy znak gracza
void put(board* brd, int circular) {
	turn p_turn = enter_turn(brd);
	if (circular % 2 == 1)	brd->b_pointer[p_turn.verse][p_turn.column] = player;
	else if (circular % 2 == 0)	brd->b_pointer[p_turn.verse][p_turn.column] = enemy;
}
