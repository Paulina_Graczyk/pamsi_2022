﻿#include <iostream>
#include <chrono>
#include <vector>
#include <fstream>

#include "AI.h"
#include "Board.h"
#include "Interface.h"
#include "Turn.h"
#include "Player.h"

using namespace std;

int main() {
	int circular = 1;
	bool p_vs_c = false;
	bool is_left = true;

	int selection = menu();
	int size = enter_size();
	int scope = enter_scope();

	if (selection == 2) p_vs_c = true;

	board* new_board = new board(size, scope);
	new_board->show();
	while (is_left) {
		move_statement(circular);
		if (circular % 2 == 1) {
			put(new_board, circular);
			new_board->show();
		}

		else if (circular % 2 == 0) {
			if (p_vs_c) c_put(new_board);
			else put(new_board, circular);
			new_board->show();
		}

		if (new_board->check_who_wins() == 1 || new_board->check_who_wins() == -1) {
			break;
		}
		is_left = new_board->is_move_left();
		circular++;
	}
	statement_end(new_board, circular);
	new_board->~board();
}