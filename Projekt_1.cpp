﻿

#include <iostream>

using namespace std;

const int SIZE = 10;
int* temp = new int[SIZE];

//tworzenie tablicy
int* tab(int n)
{
	int* tablica = new int[SIZE];
	srand((int)time(0));

	for (int i = 0; i < n; i++) {
		tablica[i] = rand() % 100;
	}
	return tablica;

}

//sortowanie przez scalanie
void merge_sort(int* tab, int l, int r) {
	
	int p,q;

	if (r <= l) {
		return;
	} else {
		int half = (int) (( l + r) / 2);
		merge_sort(tab, l, half);
		merge_sort(tab, half + 1, r);

		for (int i = l; i <= r; i++) { //skopiowanie do tablicy pomocniczej
			temp[i] = tab[i];
		}

		p = l;
		q = half+1;

		//scalanie
		for (int i = l; i <= r; i++) {
			if (p <= half) {
				if (q <= r) {
					if (temp[p] <= temp[q]) {
						tab[i] = temp[p];
						p++;
					}
					else {
						tab[i] = temp[q];
						q++;
					}
				}
				else {
					tab[i] = temp[p];
					p++;
				}
			}
			else {
				temp[i] = temp[q];
				q++;
			}
		}
	}
}


//sortowanie szybkie
void quick_sort(int* tab, int l, int r, bool flag)
{

	int  p = tab[(l + r) / 2];
	int i = l;
	int j = r;
	bool t;

	if (flag) {
		do {

			while (tab[i] < p) {
				i++;
			}
			while (tab[j] > p) {
				j--;
			}

			if (i <= j) {
				swap(tab[i], tab[j]);
				i++;
				j--;
			}

		} while (i <= j);
		if (j > l)
			quick_sort(tab, l, j,true);
		if (i < r)
			quick_sort(tab, i, r,true);
	}
	else {
		do {

			while (tab[i] > p) {
				i++;
			}
			while (tab[j] < p) {
				j--;
			}

			if (i <= j) {
				swap(tab[i], tab[j]);
				i++;
				j--;
			}

		} while (i <= j);
		if (j > l)
			quick_sort(tab, l, j,false);
		if (i < r)
			quick_sort(tab, i, r,false);
	}

	
}


void heapify(int* tab, int rozm, int i) {
	// znajdoawnie najwiekszego z sposrod korzenia i galezi
	int largest = i;
	int left = 2 * i + 1;
	int right = 2 * i + 2;

	if (left < rozm && tab[left] > tab[largest])
		largest = left;

	if (right < rozm && tab[right] > tab[largest])
		largest = right;


	if (largest != i) { //kontynuuj dopuki korzeń nie jest najwiekszy
		swap(tab[i], tab[largest]);
		heapify(tab, rozm, largest);
	}
}

//sortowanie przez kopcowanie
void heap_sort(int* tab) {
	//budowanie kopca
	for (int i = SIZE / 2 - 1; i >= 0; i--)
		heapify(tab, SIZE, i);

	// Heap sort
	for (int i = SIZE - 1; i >= 0; i--) {
		swap(tab[0], tab[i]);
		heapify(tab, i, 0);
	}
}

//wyswietlanie tablicy
void show_tab(int* tab){


	for (int i=0; i < SIZE; i++){
		cout << tab[i] << endl;
	}

}

void check_sort(int* tab) {
	bool pom;
	for (int i = 0; i < SIZE-1; i++) {
		if (tab[i + 1] < tab[i]) {
			pom = true;
		}
		else {
			pom = false;
		}
	}

	if (pom) {
		cout << "Posortowwano niepoprawnie!" << endl;
	}
	else {
		cout << "Posortowano poprawnie, wszystko ok" << endl;
	}
}

int main(){
	//int* tablica[100];
	//for (int i = 0; i < 100; i++) {
	//	int* tab1 = tab(SIZE);
	////	merge_sort(tab1, 0, SIZE * 0.95); //czesciowe sortowanie
	//	quick_sort(tab1, 0, SIZE - 1, false); //odwrotne sortowanie
	//	tablica[i] = tab1;
	//}
	int* tab1 = tab(SIZE);
	cout << "przed posortowaniem:" << endl;
	show_tab(tab1);
	/*clock_t start = clock();
	for (int i = 0; i < 100; i++) {
		heap_sort(tablica[i]);
	}
	clock_t stop = clock();
	double time = double(stop - start) / CLOCKS_PER_SEC;*/
	quick_sort(tab1, 0, SIZE - 1,true);
	//heap_sort(tab1);
	cout << "po posortowaniu:" << endl;
	show_tab(tab1);
	
	//cout << "czas wykonywania: " << time << endl;

	/*for (int i = 0; i < 100; i++) {
		delete[] tablica[i];
	}
	delete[] temp;*/
}

